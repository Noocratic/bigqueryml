declare module "@salesforce/apex/BigQueryController.runBigQuerySql" {
  export default function runBigQuerySql(param: {sql: any}): Promise<any>;
}
declare module "@salesforce/apex/BigQueryController.getVisitorNameById" {
  export default function getVisitorNameById(param: {visitorId: any}): Promise<any>;
}
declare module "@salesforce/apex/BigQueryController.setPrediction" {
  export default function setPrediction(param: {visitorId: any, prediction: any}): Promise<any>;
}
