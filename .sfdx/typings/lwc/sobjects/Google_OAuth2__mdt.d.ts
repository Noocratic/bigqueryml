declare module "@salesforce/schema/Google_OAuth2__mdt.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.DeveloperName" {
  const DeveloperName:string;
  export default DeveloperName;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.MasterLabel" {
  const MasterLabel:string;
  export default MasterLabel;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.Language" {
  const Language:string;
  export default Language;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.NamespacePrefix" {
  const NamespacePrefix:string;
  export default NamespacePrefix;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.Label" {
  const Label:string;
  export default Label;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.QualifiedApiName" {
  const QualifiedApiName:string;
  export default QualifiedApiName;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.Issuer__c" {
  const Issuer__c:string;
  export default Issuer__c;
}
declare module "@salesforce/schema/Google_OAuth2__mdt.PKCS8__c" {
  const PKCS8__c:string;
  export default PKCS8__c;
}
