declare module "@salesforce/schema/Transaction__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Transaction__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Transaction__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Transaction__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Transaction__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Transaction__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Transaction__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Transaction__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Transaction__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Transaction__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Transaction__c.VisitorId__r" {
  const VisitorId__r:any;
  export default VisitorId__r;
}
declare module "@salesforce/schema/Transaction__c.VisitorId__c" {
  const VisitorId__c:any;
  export default VisitorId__c;
}
declare module "@salesforce/schema/Transaction__c.Total__c" {
  const Total__c:number;
  export default Total__c;
}
