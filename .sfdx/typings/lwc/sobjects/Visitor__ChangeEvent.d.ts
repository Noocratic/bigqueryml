declare module "@salesforce/schema/Visitor__ChangeEvent.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.ReplayId" {
  const ReplayId:string;
  export default ReplayId;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.ChangeEventHeader" {
  const ChangeEventHeader:any;
  export default ChangeEventHeader;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.OS__c" {
  const OS__c:string;
  export default OS__c;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.Is_Mobile__c" {
  const Is_Mobile__c:boolean;
  export default Is_Mobile__c;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.Country__c" {
  const Country__c:string;
  export default Country__c;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.PageViews__c" {
  const PageViews__c:number;
  export default PageViews__c;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.NumberOfTransactions__c" {
  const NumberOfTransactions__c:number;
  export default NumberOfTransactions__c;
}
declare module "@salesforce/schema/Visitor__ChangeEvent.TotalSpending__c" {
  const TotalSpending__c:number;
  export default TotalSpending__c;
}
