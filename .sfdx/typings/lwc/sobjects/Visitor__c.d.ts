declare module "@salesforce/schema/Visitor__c.Id" {
  const Id:any;
  export default Id;
}
declare module "@salesforce/schema/Visitor__c.Owner" {
  const Owner:any;
  export default Owner;
}
declare module "@salesforce/schema/Visitor__c.OwnerId" {
  const OwnerId:any;
  export default OwnerId;
}
declare module "@salesforce/schema/Visitor__c.IsDeleted" {
  const IsDeleted:boolean;
  export default IsDeleted;
}
declare module "@salesforce/schema/Visitor__c.Name" {
  const Name:string;
  export default Name;
}
declare module "@salesforce/schema/Visitor__c.CreatedDate" {
  const CreatedDate:any;
  export default CreatedDate;
}
declare module "@salesforce/schema/Visitor__c.CreatedBy" {
  const CreatedBy:any;
  export default CreatedBy;
}
declare module "@salesforce/schema/Visitor__c.CreatedById" {
  const CreatedById:any;
  export default CreatedById;
}
declare module "@salesforce/schema/Visitor__c.LastModifiedDate" {
  const LastModifiedDate:any;
  export default LastModifiedDate;
}
declare module "@salesforce/schema/Visitor__c.LastModifiedBy" {
  const LastModifiedBy:any;
  export default LastModifiedBy;
}
declare module "@salesforce/schema/Visitor__c.LastModifiedById" {
  const LastModifiedById:any;
  export default LastModifiedById;
}
declare module "@salesforce/schema/Visitor__c.SystemModstamp" {
  const SystemModstamp:any;
  export default SystemModstamp;
}
declare module "@salesforce/schema/Visitor__c.LastViewedDate" {
  const LastViewedDate:any;
  export default LastViewedDate;
}
declare module "@salesforce/schema/Visitor__c.LastReferencedDate" {
  const LastReferencedDate:any;
  export default LastReferencedDate;
}
declare module "@salesforce/schema/Visitor__c.OS__c" {
  const OS__c:string;
  export default OS__c;
}
declare module "@salesforce/schema/Visitor__c.Is_Mobile__c" {
  const Is_Mobile__c:boolean;
  export default Is_Mobile__c;
}
declare module "@salesforce/schema/Visitor__c.Country__c" {
  const Country__c:string;
  export default Country__c;
}
declare module "@salesforce/schema/Visitor__c.PageViews__c" {
  const PageViews__c:number;
  export default PageViews__c;
}
declare module "@salesforce/schema/Visitor__c.NumberOfTransactions__c" {
  const NumberOfTransactions__c:number;
  export default NumberOfTransactions__c;
}
declare module "@salesforce/schema/Visitor__c.TotalSpending__c" {
  const TotalSpending__c:number;
  export default TotalSpending__c;
}
