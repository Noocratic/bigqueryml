({
    init : function(component, event, helper) {
       
       
        helper.getVisitorNameById(component).then(result => { 
            var action = component.get("c.runBigQuerySql");
            var query = "SELECT Name, TransactionsQuantity as Purchases, DataType, SUM(predicted_label) as PredictedPurchases " +
                        "FROM ML.PREDICT(MODEL `bigquery-ml-300911.TransactionsHistory.transactionsPredictor`," +  
                        "(SELECT Name AS Name, DataType AS DataType, TransactionsQuantity AS TransactionsQuantity," +   
                        "OS AS OS, IsMobile AS IsMobile, Country AS Country, IFNULL(PageViews, 0) AS PageViews, IFNULL(TotalSpending, 0) AS TotalSpending " +  
                        "FROM `bigquery-ml-300911.TransactionsHistory.SalesforceTransactions` WHERE Name = " +  "'" + result + "'" + "))" + 
                        "GROUP BY Name, TransactionsQuantity, DataType ORDER BY PredictedPurchases DESC";
            
            action.setParams({
                sql: query
            });
            $A.enqueueAction(action);
        
            action.setCallback(this, function(response) {
                if (response.getState()==='SUCCESS') {
                    helper.getPredictedValue(component, response.getReturnValue());
                    helper.updatePredictedValue(component, component.get("v.predictedValue"));
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "title": "Error",
                        "message": response.getError()[0].message
                    });
                    toastEvent.fire();
                }
            });
        });  
    },
})
