({
    getVisitorNameById: function(component) {
        return new Promise(
            $A.getCallback(function(resolve, reject) {
                var action = component.get("c.getVisitorNameById"); 
                action.setParams({
                visitorId: component.get("v.recordId")      
                });
        
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    
                if(component.isValid() && state == "SUCCESS"){
                    resolve(response.getReturnValue());
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "title": "Error",
                        "message": response.getError()[0].message
                    });
                    toastEvent.fire();
                }
                });
            $A.enqueueAction(action);
            })
        );
    },
    updatePredictedValue: function(component, predictedValue){
        var action = component.get("c.setPrediction"); 
        action.setParams({
            visitorId: component.get("v.recordId"),
            prediction: predictedValue
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS" && response.getReturnValue() == 'Success'){
                if(response.getReturnValue() == "Success"){
                    console.log('value updated');
                } else {
                    console.log('value not updated, because no user was found.');
                }   
                //component.set('v.statusMessage', response.getReturnValue());
            }
            else if(component.isValid() && state == "SUCCESS" && response.getReturnValue() != 'Success'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error",
                    "message":response.getReturnValue(),
                });
                toastEvent.fire();
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error",
                    "message": response.getError()[0].message,
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },

    getPredictedValue: function(component, response) {
        const jsonResponse = JSON.parse(response);
        let index = 0;
        let predictionIndex;
        
        jsonResponse.schema.fields.forEach(field => {
            if(field.name === 'PredictedPurchases'){
                predictionIndex = index;
            } else {
                index++;
            }
            
        });
        component.set('v.predictedValue', Math.round(jsonResponse.rows[0].f[predictionIndex].v));
    },
})
