public with sharing class BigQueryController {
    public BigQueryController() {

    }

    @AuraEnabled
    public static String runBigQuerySql(String sql){

        String requestType = Constants.QUERY_OBJECTS;

        GoogleBigQuery google = new GoogleBigQuery();

        // prepare query
        GoogleBigQuery.JobsQuery queryJob = new GoogleBigQuery.JobsQuery(sql);

        // make callout to BigQuery
        google.makeCallout(queryJob, requestType);

        if (!google.isSuccess()) {
            throw new AuraHandledException('Error when Querying: ' + google.getResponse());
        }
        return google.getResponse();
    }

    @AuraEnabled
    public static String getVisitorNameById(Id visitorId) {
        List<Visitor__c> visitors = [SELECT Name FROM Visitor__c WHERE Id = :visitorId];
        return !visitors.isEmpty() ? visitors[0].Name : '';
    }

    //write function to set returned prediction to a field
    @AuraEnabled
    public static String setPrediction(Id visitorId, Integer prediction) {
        List<Visitor__c> visitors = [SELECT PredictedTransactions__c FROM Visitor__c WHERE Id = :visitorId];
        if(!visitors.isEmpty()){
            visitors[0].PredictedTransactions__c = prediction;
            try{
                update visitors;
            } catch(DmlException e) {
                visitors[0].addError('Prediction field update failed: ' + e.getMessage());
            }
            return 'Success';
        } else {
            return 'This Visitor does not exist in Salesforce';
        }
    }
}
