public with sharing class Constants {

    // Request types to BigQuery
    public static final String INSERT_OBJECTS = 'insert';
    public static final String QUERY_OBJECTS = 'query';

    // Certification name stored in Salesforce that is generated from GCP keys
    public static final String CERTIFICATION = 'GCP';

    // Error messages
    public static final String NO_CUSTOM_METADATA = 'No custom metadata type record found for authorization';
}
