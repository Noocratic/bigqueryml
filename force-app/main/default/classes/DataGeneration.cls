public class DataGeneration {
    
    /*
    public DataGeneration(Integer visitorsAmount, String dataType){
        this.visitorsAmount = visitorsAmount;
        this.dataType = dataType;
        visitors = new List<Visitor__c>();
        transactions = new List<Transaction__c>();
        errorMessage = '';
    }
    */
    /*
    public void generateData(){
        String errorMessage;
        generateVisitors();
        if(dataType != 'Testing'){
            generateTransactions();
        }
        setPageViews();

        if(errorMessage == ''){
            System.debug('Success');
        } else {
            System.debug('We have a problem Sir: ' + errorMessage);
        }
        
    }*/
    
    public static GenerationResult generateVisitors(Integer visitorsAmount, String dataType){
        List<Visitor__c> visitors = new List<Visitor__c>();
        String errorMessage = '';
        for(Integer i = 0; i < visitorsAmount; i++){
            String UUID;
            try{
                UUID =  generateUUID();
            } 
            catch(Exception exc){
                errorMessage = 'Unique ID was not generated for the user: ' + exc.getMessage();
                return new GenerationResult(visitors, errorMessage);
            }
            if(i < visitorsAmount * 0.25){
                visitors.add(new Visitor__c(
                    Name = 'Visitor_' + UUID,
                    OS__c = 'Android',
                    IsMobile__c = true,
                    Country__c = 'Zimbabwe',
                    dataType__c = dataType
            	));
            } 
            else if(i >= visitorsAmount  * 0.25 && i <= visitorsAmount  * 0.5){
                visitors.add(new Visitor__c(
                    Name = 'Visitor_' + UUID,
                    OS__c = 'iOS',
                    IsMobile__c = true,
                    Country__c = 'United States',
                    dataType__c = dataType
            	));
            }
            else if(i < visitorsAmount * 0.75 && i > visitorsAmount  * 0.5){
                visitors.add(new Visitor__c(
                    Name = 'Visitor_' + UUID,
                    OS__c = 'Windows',
                    IsMobile__c = false,
                    Country__c = 'United States',
                    dataType__c = dataType
            	));
            }
            else if(i >= visitorsAmount * 0.75){
                visitors.add(
                    new Visitor__c(
                        Name = 'Visitor_' + UUID,
                        OS__c = 'Windows',
                        IsMobile__c = false,
                        Country__c = 'Latvia',
                        dataType__c = dataType
            	));
            }
        } 
        try{
             insert visitors;
        } catch(DmlException e){
            errorMessage = 'visitors with DataType' + dataType + ' were not created: ' + e.getMessage();
            return new GenerationResult(new List<Visitor__c>(), errorMessage);
        }
        return new GenerationResult(visitors, errorMessage);
    }
    
    public static GenerationResult generateTransactions(List<Visitor__c> visitors){
        String errorMessage = '';
        List<Transaction__c> transactions = new List<Transaction__c>();
        Integer randomNumber;
        for(Visitor__c visitor : visitors){

            // method returns numbers from 0 to 8
            randomNumber = Integer.valueof((Math.random() * 8));
            
            for(Integer i = 0; i < randomNumber; i++){
                transactions.add(
                	new Transaction__c(
                    	Total__c = Integer.valueof((Math.random() * 150)) + 0.1,
                        VisitorId__c = visitor.Id,
                        Name = visitor.Id + '_' + i
                    ));
            }
        }
        try{
             insert transactions;
        } catch(DmlException e){
            errorMessage = 'transactions were not created: ' + e.getMessage();
        }
        return new GenerationResult(null, errorMessage);
    }
    
    public static GenerationResult setPageViews(List<Visitor__c> visitors){
        String errorMessage = '';
        Integer randomCoefficient;
        Set<Id> visitorsIds = new Set<Id>();
        for(Visitor__c visitor : visitors){
            visitorsIds.add(visitor.Id);
        }

        visitors = [SELECT Id, PageViews__c, TransactionsQuantity__c FROM Visitor__c WHERE Id = :visitorsIds];
        for(Visitor__c visitor : visitors){
            // method returns numbers from 1 to 7
            randomCoefficient = Integer.valueof((Math.random() * 6) + 1); 
            if(visitor.TransactionsQuantity__c != null  && visitor.TransactionsQuantity__c != 0){
                visitor.PageViews__c = visitor.TransactionsQuantity__c * randomCoefficient;
            } else {
                visitor.PageViews__c = randomCoefficient;
            }
            
        }
        try{
            update visitors;
       } catch(DmlException e){
           errorMessage = 'visitors page views were not set: ' + e.getMessage();
       }
       return new GenerationResult(null, errorMessage);
    }

    // Method generates unique Name for Visitor
    public static String generateUUID() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        // I don't need a full key, first part is enough
        String guid = h.SubString(0,12);
        return guid;
    }

    public class GenerationResult{
        public List<Sobject> records;
        public String errorMessage;

        public GenerationResult(List<Sobject> records, String errorMessage){
            this.records = records;
            this.errorMessage = errorMessage;
        }
    }
}