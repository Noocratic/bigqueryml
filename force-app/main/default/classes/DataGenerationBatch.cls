public class DataGenerationBatch implements Database.Batchable<sObject>, Database.Stateful {

    public final Integer visitorsAmount;
    //List<Transaction__c> transactions;
    public List<String> errorMessages;
    
    public DataGenerationBatch(Integer visitorsAmount){
        errorMessages = new List<String>();
        this.visitorsAmount = visitorsAmount;
    }

    public Iterable<sObject> start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        List<Visitor__c> visitors = new List<Visitor__c>();
        List<DataGeneration.GenerationResult> grList = new List<DataGeneration.GenerationResult>();
        grList.add(DataGeneration.generateVisitors(visitorsAmount, 'Testing'));
        grList.add(DataGeneration.generateVisitors(visitorsAmount, 'Training'));
        grList.add(DataGeneration.generateVisitors(visitorsAmount, 'Evaluation'));
        for(DataGeneration.GenerationResult gr : grList){
            if(String.isNotBlank(gr.errorMessage)){
                errorMessages.add(gr.errorMessage);
            }
            visitors.addAll((List<Visitor__c>)gr.records);
        }
        return visitors;
    }
    public void execute(Database.BatchableContext bc, List<Visitor__c> visitors){
        // process each batch of records
        List<Visitor__c> VisitorsWithTransactions = new List<Visitor__c>();
        For(Visitor__c vis : visitors){
            if(vis.DataType__c != 'Testing'){
                VisitorsWithTransactions.add(vis);
            }
        }

        List<DataGeneration.GenerationResult> grList = new List<DataGeneration.GenerationResult>();
        grList.add(DataGeneration.setPageViews(visitors));
        grList.add(DataGeneration.generateTransactions(VisitorsWithTransactions));

        for(DataGeneration.GenerationResult gr : grList){
            if(String.isNotBlank(gr.errorMessage)){
                errorMessages.add(gr.errorMessage);
            }
        }
    }
    public void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        if(errorMessages.isEmpty()){
            System.debug('Success');
        } else {
            System.debug('The following errors during data generation occured:');
            for(String errorMessage : errorMessages){
                System.debug(errorMessage);
            }    
        }
    }
}