public class Futures {

    public Futures() {
    }

    @future(callout=true)
    public static void sendVisitorToGoogleBigQuery(Integer quantity) {
        String requestType = Constants.INSERT_OBJECTS;
        List<Visitor__c> visitors = [SELECT Name, DataType__c, Country__c, IsMobile__c,
                                    TransactionsQuantity__c, OS__c, PageViews__c, TotalSpending__c 
                                        FROM Visitor__c limit :quantity];
        if (!visitors.isEmpty()) {
            // Setup connector
            GoogleBigQuery google = new GoogleBigQuery();
            GoogleBigQuery.InsertAll insertAll = new GoogleBigQuery.InsertAll();

            for(Visitor__c visitor : visitors){
                insertAll.addObject(visitor);
            }
            
            //make callout to BigQuery
            google.makeCallout(insertAll, requestType);
           
            if (!google.isSuccess()) {
               System.debug('Error: ' + google.getResponse());
            }
        }
    }


}