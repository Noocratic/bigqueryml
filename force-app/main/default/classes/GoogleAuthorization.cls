public with sharing class GoogleAuthorization {

    private String accessToken;
    private String errorMessage;

    public GoogleAuthorization() {
        accessToken = '';
        errorMessage = '';
        authorize();
    }

    public void authorize(){
        System.debug('authorize');
      
        List<GCP_Auth2__mdt> mdts = [SELECT Issuer__c, Audition__c, Scope__c, tokenEndpoint__c FROM GCP_Auth2__mdt WHERE DeveloperName = 'BigQuery'];
        
        if (!mdts.isEmpty()) {
            GCP_Auth2__mdt mdt = mdts.get(0);
            String issuer = mdt.Issuer__c;
            String audition = mdt.Audition__c;
            String scope = mdt.Scope__c;
            String tokenEndpoint = mdt.tokenEndpoint__c;

            Auth.JWT jwt = new Auth.JWT();
            jwt.setSub(issuer); 
            jwt.setAud(audition); 
            jwt.setIss(issuer);

            //Additional claims to set scope
            Map<String, Object> claims = new Map<String, Object>();
            claims.put('scope', scope);
                
            jwt.setAdditionalClaims(claims);

            //Create the object that signs the JWT bearer token with a certificaiton
            Auth.JWS jws = new Auth.JWS(jwt, Constants.CERTIFICATION);

            //Get the resulting JWS in case debugging is required
            String token = jws.getCompactSerialization();

            //POST the JWT bearer token to the defined token endpoint
            Auth.JWTBearerTokenExchange bearer = new Auth.JWTBearerTokenExchange(tokenEndpoint, jws);

            //Get the access token
            this.accessToken = bearer.getAccessToken();

            //Get response
            HTTPResponse res = bearer.getHttpResponse();
            
            if (res.getStatusCode() != 200){
                this.errorMessage = res.getStatus();
            }
        } else {
            this.errorMessage = Constants.NO_CUSTOM_METADATA;
        }
    }

    public String getAccessToken(){
        return this.accessToken;
    }

    public String getErrorMessage(){
        return this.errorMessage;
    }
}

