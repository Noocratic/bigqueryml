public class GoogleBigQuery {

    private String projectId; // Project ID from Google BigQuery
    private String datasetId; // Dataset ID from Google BigQuery
    private String tableId; // Table ID from Google BigQuery
    private Boolean isSuccess = false;
    private String response;

    public GoogleBigQuery() {
        List<GCP_Configuration__mdt> mdts = [SELECT projectId__c, datasetId__c, tableId__c FROM GCP_Configuration__mdt WHERE DeveloperName = 'BigQuery'];
        if (!mdts.isEmpty()) {
            GCP_Configuration__mdt mdt = mdts.get(0);
            this.datasetId = mdt.datasetId__c;
            this.projectId = mdt.projectId__c;
            this.tableId = mdt.tableId__c;
        }
    }

    public GoogleBigQuery(String projectId, String datasetId, String tableId) {
        this.datasetId = datasetId;
        this.projectId = projectId;
        this.tableId = tableId;
    }

    public void makeCallout(Object data, String type) {

        if(this.projectId != null && this.projectId != ''){

            GoogleAuthorization auth = new GoogleAuthorization(); // Custom Google OAuth Provider
            
            if (auth.getErrorMessage() == null || auth.getErrorMessage() == '') {

                System.debug('=== make callout with type ' + type);
                Request req = new Request (auth.getAccessToken(), type, projectId, datasetId, tableId);
                req.send(data);
                this.isSuccess = req.isSuccess();
                this.response = req.getResponse();
                System.debug('=== response status ' + req.isSuccess());
                System.debug('=== response ' + req.getResponse());
            } else {
                this.response = auth.getErrorMessage();
            }
            
        } else {
            this.response = Constants.NO_CUSTOM_METADATA;
        }
    }

    public Boolean isSuccess() {
        return this.isSuccess;
    }

    public String getResponse() {
        return this.response;
    }

    // Internal class for handling communication with Google BigQuery
    class Request {

        private String accessToken;
        private String requestType;
        private String baseUrl = 'https://www.googleapis.com/bigquery/v2/projects/projectId/';
        private String projectId;
        private String datasetId;
        private String tableId;
        private Boolean isSuccess = false;
        private String response;

        Request(String accessToken, String type, String projectId) {
            this.accessToken = accessToken;
            this.requestType = type;
            this.projectId = projectId;
        }

        Request(String accessToken, String type, String projectId, String datasetId) {
            this.accessToken = accessToken;
            this.requestType = type;
            this.projectId = projectId;
            this.datasetId = datasetId;
        }

        Request(String accessToken, String type, String projectId, String datasetId, String tableId) {
            this.accessToken = accessToken;
            this.requestType = type;
            this.projectId = projectId;
            this.datasetId = datasetId;
            this.tableId = tableId;
        }

        public void send(Object data) {
            String url = '';
            String jsonData = '';

            switch on this.requestType {
                when 'query' {
                    this.baseUrl = this.baseUrl + 'queries';
                }
                when 'insert' {
                    this.baseUrl = this.baseUrl + 'datasets/datasetId/tables/tableId/insertAll';
                }
            }

            this.baseUrl = this.baseUrl.replace('projectId', projectId);
            if (datasetId != null) {
                this.baseUrl = this.baseUrl.replace('datasetId', datasetId);
            }
            if (tableId != null) {
                this.baseUrl = this.baseUrl.replace('tableId', tableId);
            }

            jsonData = System.JSON.serialize(data);
            system.debug('request body' + jsonData);
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(this.baseUrl);
            req.setHeader('Content-type', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + this.accessToken);
            req.setBody(jsonData);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            this.response = res.getBody();
            if (res.getStatusCode() == 200) {
                this.isSuccess = true;
            } else {
                Map<String, Object> errorResponse = (Map<String, Object>)JSON.deserializeUntyped(this.response);
                Map<String, Object> error = (Map<String, Object>)errorResponse.get('error');
                this.response = (String)error.get('message');
            }
        }

        public Boolean isSuccess() {
            return this.isSuccess;
        }

        public String getResponse() {
            return this.response;
        }

    }

    // Class for constructing a simple query
    public class JobsQuery {
        
        Object query;
        Boolean useLegacySql;

        public JobsQuery(String query){
            this.query = (Object) query;
            this.useLegacySql = false;
        }
    }

    // Class for constructing an insert call
    public class InsertAll {
        String kind = 'bigquery#tableDataInsertAllRequest';
        Boolean skipInvalidRows = false;
        Boolean ignoreUnknownValues = true;
        List<Object> rows = new List<Object>();

        public void addObject(Object data) {

            // remove custom prefix from fields names
            String jsonSerialized =  JSON.serialize(data).replace('__c', '');      
            Object JSONDeserialized = JSON.deserializeUntyped(jsonSerialized);
            Map<String, Object> details = new Map<String, Object>();
            details.put('json', JSONDeserialized);
            rows.add(details);
        }
    }

}